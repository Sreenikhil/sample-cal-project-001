--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------;
--	fn_sql_aux_total_raster_comb_app
--------------------------------------------------------------------------------;

DROP FUNCTION IF EXISTS @extschema@.fn_sql_aux_total_raster_comb_app(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
(
	config_id			integer,
	schema_name			character varying,
	table_name			character varying,
	column_name			character varying,
	band				integer,
	reclass				integer,
	condition			character varying,
	unit				double precision,
	schema_name_1			character varying,
	table_name_1			character varying,
	column_name_1			character varying,
	band_1				integer,
	reclass_1			integer,
	condition_1			character varying,
	unit_1				double precision,
	gid				character varying
)
RETURNS text
LANGUAGE plpgsql
IMMUTABLE
SECURITY INVOKER
AS
$$
DECLARE
	reclass_text		text;
	reclass_val_column	text;
	result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_total_raster_comb_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_total_raster_comb_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_name
	IF ((column_name IS NULL) OR (trim(column_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_total_raster_comb_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- band
	IF (band IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_total_raster_comb_app: Hodnota parametru band nesmí být NULL.';
	END IF;

	-- condition [povoleno NULL]
	IF ((condition IS NULL) OR (trim(condition) = ''))
	THEN
		condition := NULL;
	END IF;

	-- unit
	IF (unit IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_total_raster_comb_app: Hodnota parametru unit nesmí být NULL.';
	END IF;

	-- schema_name_1
	IF ((schema_name_1 IS NULL) OR (trim(schema_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_total_raster_comb_app: Hodnota parametru schema_name_1 nesmí být NULL.';
	END IF;

	-- table_name_1
	IF ((table_name_1 IS NULL) OR (trim(table_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_sql_aux_total_raster_comb_app: Hodnota parametru table_name_1 nesmí být NULL.';
	END IF;

	-- column_name_1
	IF ((column_name_1 IS NULL) OR (trim(column_name_1) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_sql_aux_total_raster_comb_app: Hodnota parametru column_name_1 nesmí být NULL.';
	END IF;

	-- band_1
	IF (band_1 IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_sql_aux_total_raster_comb_app: Hodnota parametru band_1 nesmí být NULL.';
	END IF;

	-- condition_1 [povoleno NULL]
	IF ((condition_1 IS NULL) OR (trim(condition_1) = ''))
	THEN
		condition_1 := NULL;
	END IF;

	-- unit_1
	IF (unit_1 IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 10: fn_sql_aux_total_raster_comb_app: Hodnota parametru unit_1 nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro vypocet uhrnu
	result :=
	'
	WITH
	---------------------------------------------------------------
	w_cell AS	(-- vyber uzemi
			SELECT
				geom AS geom_cell,
				estimation_cell
			FROM
				@extschema@.f_a_cell
			WHERE
				gid = #GID#
			)
	---------------------------------------------------------------
	,w_cell_rast2 AS materialized
			(-- protnuti vybraneho uzemi z rasterem
			SELECT
				t1.geom_cell,
				t1.estimation_cell,
				t2.#COLUMN_NAME# AS rast4dump
			FROM
				w_cell AS t1
			LEFT
			JOIN
				#SCHEMA_NAME#.#TABLE_NAME# AS t2

			ON	t1.geom_cell && st_convexhull(t2.#COLUMN_NAME#)
			
			AND	ST_Intersects(t1.geom_cell, st_convexhull(t2.#COLUMN_NAME#))
			
			AND	#CONDITION#
			)
	---------------------------------------------------------------
	,w_polygons AS materialized
			(-- rozdumpovani rasteru na polygony (nemusi byt pokryt cely katastr, kvuli 0 tileum)
			SELECT
				geom_cell,
				estimation_cell,
				ST_DumpAsPolygons(rast4dump, #BAND#) AS rast_poly
			FROM
				w_cell_rast2
			WHERE
				rast4dump IS NOT NULL
			)
	---------------------------------------------------------------
	,w_dumps AS materialized
			(
			SELECT
				geom_cell,
				estimation_cell,
				(rast_poly).val,
				(rast_poly).geom
			FROM
				w_polygons 
			WHERE
				#RECLASS#
			)
	---------------------------------------------------------------
	,w_intersection AS materialized
			(
			SELECT
				geom_cell,
				estimation_cell,
				#RECLASS_VAL_COLUMN# * #UNIT# AS val_reclass,				
				ST_Intersection (geom_cell, geom) AS geom
			FROM
				w_dumps
			)
	---------------------------------------------------------------
	,w_cell_rast1 as materialized
			(-- protnuti rasteru1 z rozdampovanyma geometriema withu w_intersection
			SELECT
				geom_cell,
				estimation_cell,
				val_reclass,
				ST_Intersection(t1.geom, t2.#COLUMN_NAME_1#) AS res_intersects
			FROM
				w_intersection AS t1
			LEFT JOIN
				#SCHEMA_NAME_1#.#TABLE_NAME_1# AS t2
			
			ON	t1.geom && st_convexhull(t2.#COLUMN_NAME_1#)
			
			AND	ST_Intersects(t1.geom, st_convexhull(t2.#COLUMN_NAME_1#))
			
			AND	#CONDITION_1#
			
			WHERE
				ST_IsEmpty(t1.geom) = false
			)
	---------------------------------------------------------------
	,w_dump_all as materialized
			(-- rozbaleni intersection ndsm na geometrii a hodnotu
			SELECT 
				estimation_cell,
				val_reclass,
				(res_intersects).geom AS res_geom,
				(res_intersects).val AS ndsm_val
			FROM
				w_cell_rast1
			)
	---------------------------------------------------------------
	,w_sum_part AS	(
			select
				sum((ST_Area(res_geom)) * val_reclass * ndsm_val) as sum_part
			FROM
				w_dump_all
			)
	---------------------------------------------------------------
	,w_sum AS 	(
			-- tady se protnul ndsm s ftype
			-- tam kde cast katastru byla pokryta ftype 0 (chybeji tile) se neprovede nic,
			-- tyto pixely jsou odstraneny jiz na zacatku
			
			SELECT 
				sum_part
			FROM 
				w_sum_part
			-------------------------------------
			UNION ALL
			-------------------------------------
			-- pripojeni katastru, ktere maji ftype vsude 0
			
			SELECT
				0.0 AS sum_part
			FROM
				w_cell_rast2
			WHERE
				rast4dump IS NULL
			-------------------------------------
			UNION ALL
			-------------------------------------
			-- pripojeni geometrie, ktere jsou pokryte nejakym tilem, ale vsude je 0

			SELECT
				0.0 AS sum_part
			FROM
				w_polygons
			WHERE
				(rast_poly).val = 0
			)
	---------------------------------------------------------------
	SELECT 
		coalesce(sum(sum_part),0) AS aux_total_#CONFIG_ID#_#GID#
	FROM 
		w_sum;
	';
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace pro raster v prvnim poradi protinani
	IF (reclass IS NULL)
	THEN
		reclass_text := '(rast_poly).val != 0';			-- defaultni nastaveni
		reclass_val_column := 'val';				-- val hodnota se zde nepreklasifikovava [bere se sloupec val]
	ELSE
		reclass_text := '(rast_poly).val = #RECLASS_VALUE#';	-- hodnota val urci s jakymi rozdampovanymi polygony se bude dale pracovat [ty si nesou hodnotu val z prvniho protinani]
		reclass_val_column := '1.0';				-- hodnota val z prvniho protinani se umele preklasifikuje vzdy na hodnotu 1.0
	END IF;
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace pro raster ve druhem poradi protinani
	IF (reclass_1 IS NOT NULL)
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_sql_aux_total_raster_comb_app: Neznama reklasifikace pro druhy raster v protinani!'; -- momentalne jako raster pro druhe protinani je vrstva ndsm u ktere se reklasifikace neprovadi
	END IF;
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	result := replace(result, '#RECLASS#', reclass_text);
	result := replace(result, '#RECLASS_VAL_COLUMN#', reclass_val_column);
	result := replace(result, '#CONFIG_ID#', config_id::character varying);
	result := replace(result, '#SCHEMA_NAME#', schema_name);
	result := replace(result, '#TABLE_NAME#', table_name);
	result := replace(result, '#COLUMN_NAME#', column_name);
	result := replace(result, '#BAND#', band::character varying);	
	result := replace(result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	result := replace(result, '#UNIT#', unit::character varying);
	result := replace(result, '#SCHEMA_NAME_1#', schema_name_1);
	result := replace(result, '#TABLE_NAME_1#', table_name_1);
	result := replace(result, '#COLUMN_NAME_1#', column_name_1);
	result := replace(result, '#BAND_1#', band_1::character varying);
	result := replace(result, '#CONDITION_1#', coalesce(condition_1::character varying, 'TRUE'));
	result := replace(result, '#UNIT_1#', unit_1::character varying);
	result := replace(result, '#GID#', gid);

	IF (reclass IS NOT NULL)
	THEN
		result := replace(result, '#RECLASS_VALUE#', reclass::character varying);
	END IF;
	-----------------------------------------------------------------------------------
	RETURN result;
	-----------------------------------------------------------------------------------
END;
$$;

--------------------------------------------------------------------------------;

ALTER FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
OWNER TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO app_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_total_raster_comb_app
	(
	integer,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying, character varying, character varying, integer, integer, character varying, double precision,
	character varying
	)
IS
'Funkce vrací SQL textový řetězec pro výpočet úhrnu pomocné proměnné z rasterové kategorie v rámci vektorové vrstvy.';

--------------------------------------------------------------------------------;
