--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_check_cell_sup_app();

CREATE OR REPLACE FUNCTION @extschema@.fn_check_cell_sup_app()
RETURNS void AS
$BODY$
DECLARE
	_ec_highest	integer[];
	_check		integer;
BEGIN
	---------------------------------------------------
	SELECT array_agg(t.estimation_cell_collection_highest ORDER BY t.estimation_cell_collection_highest)
	FROM (SELECT DISTINCT estimation_cell_collection_highest FROM @extschema@.cm_estimation_cell_collection) AS t
	INTO _ec_highest;
	---------------------------------------------------
	FOR i IN 1..array_length(_ec_highest,1)
	LOOP
		WITH
		w1 AS	(
			SELECT id AS estimation_cell FROM @extschema@.c_estimation_cell
			WHERE estimation_cell_collection = _ec_highest[i]
			),
		w2 AS	(
			SELECT estimation_cell FROM @extschema@.f_a_cell WHERE gid IN
			(SELECT cell FROM @extschema@.cm_f_a_cell WHERE cell_sup IS NULL)
			AND estimation_cell IN (SELECT estimation_cell FROM w1)
			),
		w3 AS	(
			SELECT estimation_cell FROM w1 except
			SELECT estimation_cell FROM w2
			),
		w4 AS	(
			SELECT estimation_cell FROM w2 except
			SELECT estimation_cell FROM w1
			)
		SELECT count(t.*) FROM (SELECT * FROM w3 UNION ALL SELECT * FROM w4) AS t
		INTO _check;

		IF _check > 0
		THEN
			RAISE EXCEPTION 'Error 01: fn_check_cell_sup_app: For estimation_cell_collection = % miss null records in table cm_f_a_cell for column cell_sup!',_ec_highest[i];
		END IF;
			
	END LOOP;
	---------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
ALTER FUNCTION @extschema@.fn_check_cell_sup_app() OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_cell_sup_app() TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_cell_sup_app() TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_cell_sup_app() TO public;

COMMENT ON FUNCTION @extschema@.fn_check_cell_sup_app() IS
'Function checks that for all estimation_cell_collections are null records in table cm_f_a_cell for column cell_sup';
---------------------------------------------------------------------------------------------------