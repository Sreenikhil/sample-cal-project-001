--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------;
--	fn_sql_aux_total_raster_app
--------------------------------------------------------------------------------;

DROP FUNCTION IF EXISTS @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying);

CREATE OR REPLACE FUNCTION @extschema@.fn_sql_aux_total_raster_app
(
	config_id			integer,
	schema_name			character varying,
	table_name			character varying,
	column_name			character varying,
	band				integer,
	reclass				integer,
	condition			character varying,
	unit				double precision,
	gid				character varying
)
RETURNS text
LANGUAGE plpgsql
IMMUTABLE
SECURITY INVOKER
AS
$$
DECLARE
	reclass_text		text;
	result			text;
BEGIN
	-----------------------------------------------------------------------------------
	-- schema_name
	IF ((schema_name IS NULL) OR (trim(schema_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_sql_aux_total_raster_app: Hodnota parametru schema_name nesmí být NULL.';
	END IF;

	-- table_name
	IF ((table_name IS NULL) OR (trim(table_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_sql_aux_total_raster_app: Hodnota parametru table_name nesmí být NULL.';
	END IF;

	-- column_name
	IF ((column_name IS NULL) OR (trim(column_name) = ''))
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_sql_aux_total_raster_app: Hodnota parametru column_name nesmí být NULL.';
	END IF;

	-- band
	IF (band IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_sql_aux_total_raster_app: Hodnota parametru band nesmí být NULL.';
	END IF;

	-- condition [povoleno NULL]
	IF ((condition IS NULL) OR (trim(condition) = ''))
	THEN
		condition := NULL;
	END IF;

	-- unit
	IF (unit IS NULL)
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_sql_aux_total_raster_app: Hodnota parametru unit nesmí být NULL.';
	END IF;

	-- gid
	IF (gid IS NULL) OR (trim(gid) = '')
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_sql_aux_total_raster_app: Hodnota parametru gid nesmí být NULL.';
	END IF;
	-----------------------------------------------------------------------------------
	-- sestaveni dotazu pro vypocet uhrnu
	result :=
		'
		WITH 
		---------------------------------------------------------------
		w_rast_1 AS	(
				SELECT
					t1.gid, 
					t1.estimation_cell,
					t1.geom,
					CASE WHEN t2.#COLUMN_NAME# IS NULL THEN false ELSE true END AS ident_null,
					#RECLASS# AS rast_reclass,
					t2.*
				FROM
					(SELECT * FROM @extschema@.f_a_cell WHERE gid = #GID#) AS t1
				LEFT JOIN
					#SCHEMA_NAME#.#TABLE_NAME# AS t2
				ON
					t1.geom && ST_Convexhull(t2.#COLUMN_NAME#)
				AND
					ST_Intersects(t1.geom, ST_Convexhull(t2.#COLUMN_NAME#))
				)
		---------------------------------------------------------------		
		,w_rast AS	(
				SELECT
					gid,
					estimation_cell,
					geom,
					ident_null,
					rast_reclass AS rast
				FROM
					w_rast_1
				WHERE
					#CONDITION#
				)
		---------------------------------------------------------------
		,w_covers AS	(
				SELECT
					gid,
					estimation_cell,
					rast,
					geom,
					ST_Covers(geom, ST_Convexhull(rast)) AS covers
				FROM
					w_rast
				WHERE
					ident_null
				)
		---------------------------------------------------------------	
		,w_uhrn AS 	(
				SELECT
					gid,
					estimation_cell,
					(ST_ValueCount(rast, #BAND#, true)) AS val_count,
					(ST_PixelWidth(rast) * ST_PixelHeight(rast))*#UNIT# AS pixarea
				FROM
					w_covers
				WHERE
					covers = true
				)
		---------------------------------------------------------------	
		,w_uhrn_not AS	(
				SELECT
					gid,
					estimation_cell,
					ST_Intersection(geom, rast, #BAND#) AS vector_intersect_record
				FROM
					w_covers
				WHERE
					covers = false
				)
		---------------------------------------------------------------	
		,w_sum AS	(
				SELECT
					SUM(((ST_Area((vector_intersect_record).geom))*#UNIT#)*((vector_intersect_record).val)) AS sum_part
				FROM 
					w_uhrn_not
				---------------------------
				UNION ALL
				---------------------------
				SELECT
					SUM((val_count).value*(val_count).count*pixarea) AS sum_part
				FROM 
					w_uhrn 
				---------------------------
				UNION ALL
				---------------------------
				SELECT
					0.0 AS sum_part
				FROM
					w_rast
				WHERE
					ident_null = false
				)
		---------------------------------------------------------------	
		SELECT
			coalesce(sum(sum_part),0) AS aux_total_#CONFIG_ID#_#GID#
		FROM 
			w_sum;			
		';
	-----------------------------------------------------------------------------------
	-- nastaveni pripadne reklasifikace
	IF (reclass IS NULL)
	THEN
		reclass_text := 't2.#COLUMN_NAME#';
	ELSE
		reclass_text := '@extschema@.fn_get_reclass_app(t2.#COLUMN_NAME#,#BAND#,ST_BandPixelType(t2.#COLUMN_NAME#,#BAND#),#RECLASS_VALUE#)';
	END IF;
	-----------------------------------------------------------------------------------
	-- nahrazeni promennych casti v dotazu pro vypocet uhrnu
	result := replace(result, '#RECLASS#', reclass_text);
	result := replace(result, '#CONFIG_ID#', config_id::character varying);
	result := replace(result, '#SCHEMA_NAME#', schema_name);
	result := replace(result, '#TABLE_NAME#', table_name);
	result := replace(result, '#COLUMN_NAME#', column_name);
	result := replace(result, '#BAND#', band::character varying);
	result := replace(result, '#CONDITION#', coalesce(condition::character varying, 'TRUE'));
	result := replace(result, '#UNIT#', unit::character varying);
	result := replace(result, '#GID#', gid);

	IF (reclass IS NOT NULL)
	THEN
		result := replace(result, '#RECLASS_VALUE#', reclass::character varying);
	END IF;
	-----------------------------------------------------------------------------------
	RETURN result;
	-----------------------------------------------------------------------------------
END;
$$;

--------------------------------------------------------------------------------;

ALTER FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
OWNER TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
TO adm_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
TO app_nfiesta_gisdata;

GRANT EXECUTE ON FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
TO public;

COMMENT ON FUNCTION @extschema@.fn_sql_aux_total_raster_app(
	integer, character varying, character varying, character varying,
	integer, integer, character varying, double precision,
	character varying)
IS
'Funkce vrací SQL textový řetězec pro výpočet úhrnu pomocné proměnné z rasterové vrstvy.';

--------------------------------------------------------------------------------;
