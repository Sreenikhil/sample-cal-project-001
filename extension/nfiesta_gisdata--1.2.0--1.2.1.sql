--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------;
--        f_p_plot
--------------------------------------------------------------------------------;

--      SELECT * FROM @extschema@.f_p_plot;

        DROP TABLE IF EXISTS @extschema@.f_p_plot CASCADE;

        CREATE TABLE @extschema@.f_p_plot
        (
                gid                      serial,
                config_collection        integer,
                country                  character varying(20),
                strata_set               character varying(20),
                stratum                  character varying(20),
                panel                    character varying(20),
                cluster                  character varying(20),
                plot                     character varying(20),
                geom                     geometry(Point,5221)
        );

--------------------------------------------------------------------------------;
-- comments
--------------------------------------------------------------------------------;

        -- table
        COMMENT ON TABLE @extschema@.f_p_plot
                IS 'Table of inventory plots (points) for intersection with the layer of totals of auxiliary variables.';

        -- attributes
        COMMENT ON COLUMN @extschema@.f_p_plot.gid
                IS 'Generated identification number of the inventory plot record in the table f_p_plot, primary key of the table.';
        COMMENT ON COLUMN @extschema@.f_p_plot.config_collection
                IS 'Configuration group identification number, foreign key of the t_config_collection table.';
        COMMENT ON COLUMN @extschema@.f_p_plot.country
                IS 'Country identification number.';
        COMMENT ON COLUMN @extschema@.f_p_plot.strata_set
                IS 'Text designation of the stratum group.';
        COMMENT ON COLUMN @extschema@.f_p_plot.stratum
                IS 'Text designation of the stratum.';
        COMMENT ON COLUMN @extschema@.f_p_plot.panel
                IS 'Text designation of the panel.';
        COMMENT ON COLUMN @extschema@.f_p_plot.cluster
                IS 'Cluster identification number.';
        COMMENT ON COLUMN @extschema@.f_p_plot.plot
                IS 'Plot identification number.';
        COMMENT ON COLUMN @extschema@.f_p_plot.geom
                IS 'Plot coordinates, point geometry, S-JTSK.';

--------------------------------------------------------------------------------;
-- not null constraints
--------------------------------------------------------------------------------;

        -- gid
        ALTER TABLE @extschema@.f_p_plot
                ALTER COLUMN gid SET NOT NULL;

        -- config_collection
        ALTER TABLE @extschema@.f_p_plot
                ALTER COLUMN config_collection SET NOT NULL;

       -- country

       -- strata_set

       -- stratum

       -- panel

       -- cluster

       -- plot
       ALTER TABLE @extschema@.f_p_plot
                ALTER COLUMN plot SET NOT NULL;

        -- geom
       ALTER TABLE @extschema@.f_p_plot
                ALTER COLUMN geom SET NOT NULL;

--------------------------------------------------------------------------------;
-- primary key contraint
--------------------------------------------------------------------------------;

        -- f_p_plot_pkey
        ALTER TABLE @extschema@.f_p_plot
        DROP CONSTRAINT IF EXISTS pkey__f_p_plot;

        ALTER TABLE @extschema@.f_p_plot
        ADD CONSTRAINT pkey__f_p_plot
                PRIMARY KEY (gid)
                NOT DEFERRABLE
                INITIALLY IMMEDIATE;

        COMMENT ON CONSTRAINT pkey__f_p_plot
                ON @extschema@.f_p_plot
        IS 'Primary key of table f_p_plot.';

--------------------------------------------------------------------------------;
-- sequence
--------------------------------------------------------------------------------;

	-- seq__t_plots__id
	ALTER SEQUENCE @extschema@.f_p_plot_gid_seq
	RENAME TO seq__f_p_plot__gid;

	ALTER SEQUENCE @extschema@.seq__f_p_plot__gid
	MINVALUE 0;

	COMMENT ON SEQUENCE @extschema@.seq__f_p_plot__gid
	IS 'Sequence for generating the identification number of the inventory plot record in the f_p_plot table.';

--------------------------------------------------------------------------------;
-- foreign key contraints
--------------------------------------------------------------------------------;

        -- fkey__f_p_plot__t_config_collection
        ALTER TABLE @extschema@.f_p_plot
        DROP CONSTRAINT IF EXISTS fkey__f_p_plot__t_config_collection;

        ALTER TABLE @extschema@.f_p_plot
        ADD CONSTRAINT fkey__f_p_plot__t_config_collection
                FOREIGN KEY (config_collection)
                REFERENCES @extschema@.t_config_collection(id)
                MATCH SIMPLE
                ON UPDATE CASCADE
                ON DELETE NO ACTION
                NOT DEFERRABLE
                INITIALLY IMMEDIATE;

        COMMENT ON CONSTRAINT fkey__f_p_plot__t_config_collection
                ON @extschema@.f_p_plot
        IS 'Foreign key, configuration group identification number in the t_config_collection table.';

--------------------------------------------------------------------------------;
-- index
--------------------------------------------------------------------------------;

	-- fki__f_p_plot__t_config_collection
	DROP INDEX IF EXISTS @extschema@.fki__f_p_plot__t_config_collection;
	CREATE INDEX fki__f_p_plot__t_config_collection
		ON @extschema@.f_p_plot
		USING btree(config_collection);

	COMMENT ON INDEX @extschema@.fki__f_p_plot__t_config_collection
	IS 'Cover index for foreign key fkey__f_p_plot__t_config_collection.';

--------------------------------------------------------------------------------;
-- owner and privileges
--------------------------------------------------------------------------------;

	-- table

	ALTER TABLE @extschema@.f_p_plot
	OWNER TO adm_nfiesta_gisdata;

	GRANT ALL
	ON TABLE @extschema@.f_p_plot
	TO adm_nfiesta_gisdata;

	GRANT SELECT, UPDATE, DELETE, INSERT, REFERENCES, TRIGGER
	ON TABLE @extschema@.f_p_plot
	TO app_nfiesta_gisdata;

	-- sequence

	ALTER SEQUENCE @extschema@.seq__f_p_plot__gid
	OWNER TO adm_nfiesta_gisdata;

	GRANT ALL
	ON SEQUENCE @extschema@.seq__f_p_plot__gid
	TO adm_nfiesta_gisdata;

	GRANT USAGE, SELECT, UPDATE
	ON SEQUENCE @extschema@.seq__f_p_plot__gid
	TO app_nfiesta_gisdata;

--------------------------------------------------------------------------------;
-- add new version into c_ext_version and cm_ext_gui_version
--------------------------------------------------------------------------------;

	INSERT INTO @extschema@.c_ext_version(id, label, description)
	VALUES
		(500,'1.2.1','Version 1.2.1 - extension nfiesta_gisdata for auxiliary data. Add table f_p_plot.');

	INSERT INTO @extschema@.cm_ext_gui_version(ext_version, gui_version)
	VALUES
		(500,500);

--------------------------------------------------------------------------------;