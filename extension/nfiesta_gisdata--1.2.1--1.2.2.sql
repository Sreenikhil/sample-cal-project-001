--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------;
-- setting public privileges for tables
--------------------------------------------------------------------------------;

	GRANT SELECT ON TABLE @extschema@.c_config_function		TO public;
	GRANT SELECT ON TABLE @extschema@.c_config_query		TO public;
	GRANT SELECT ON TABLE @extschema@.c_estimation_cell		TO public;
	GRANT SELECT ON TABLE @extschema@.c_estimation_cell_collection	TO public;
	GRANT SELECT ON TABLE @extschema@.c_ext_version			TO public;
	GRANT SELECT ON TABLE @extschema@.c_gui_version			TO public;
	GRANT SELECT ON TABLE @extschema@.cm_estimation_cell_collection	TO public;
	GRANT SELECT ON TABLE @extschema@.cm_ext_gui_version		TO public;
	GRANT SELECT ON TABLE @extschema@.cm_f_a_cell			TO public;
	GRANT SELECT ON TABLE @extschema@.f_a_cell			TO public;
	GRANT SELECT ON TABLE @extschema@.f_p_plot			TO public;
	GRANT SELECT ON TABLE @extschema@.t_aux_total			TO public;
	GRANT SELECT ON TABLE @extschema@.t_auxiliary_data		TO public;
	GRANT SELECT ON TABLE @extschema@.t_config			TO public;
	GRANT SELECT ON TABLE @extschema@.t_config_collection		TO public;

--------------------------------------------------------------------------------;
-- add new version into c_ext_version and cm_ext_gui_version
--------------------------------------------------------------------------------;

	INSERT INTO @extschema@.c_ext_version(id, label, description)
	VALUES
		(600,'1.2.2','Version 1.2.2 - extension nfiesta_gisdata for auxiliary data. Setting public permission for tables.');

	INSERT INTO @extschema@.cm_ext_gui_version(ext_version, gui_version)
	VALUES
		(600,500);

--------------------------------------------------------------------------------;