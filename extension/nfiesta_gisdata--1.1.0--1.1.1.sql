--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------;
-- add ext_version 1.1.1
---------------------------------------------------------------------------------------------------;
INSERT INTO @extschema@.c_ext_version(id, label, description)
VALUES
	(300,'1.1.1','Verze 1.1.1 - extenze nfiesta_gisdata pro pomocná data. Vytvoření pohledu v_aux_total.');
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- add ext_version 1.1.1 into cm_ext_gui_version
---------------------------------------------------------------------------------------------------;
INSERT INTO @extschema@.cm_ext_gui_version(ext_version, gui_version)
VALUES
	(300,400);
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- VIEWS --
---------------------------------------------------------------------------------------------------;

-- v_aux_total

-- <view name="v_aux_total" schema="export_api" src="views/export_api/v_aux_total.sql">
create or replace view export_api.v_aux_total as
with
w_config as		(
			-- vyber vypocitanych config z tabulky t_aux_total
			-- a vyber potrebnych atributu, kde je hlavne
			-- dulezity atribut config_query, ktery v pripade hodnoty 500 nam 
			-- rika, ze jde o refefenci [pozn. referencni data se v tabulce]
			-- t_aux_total neduplikuji
			select
				t_config.id,
				t_config.auxiliary_variable_category,
				t_config.config_collection,
				t_config.config_query,
				t_config.label,
				t_config.categories
			from
				@extschema@.t_config
			where
				config_collection in	(
							select distinct config_collection from @extschema@.t_config
							where id in (select distinct config from @extschema@.t_aux_total)
							)
			order 
				by id
			)
,w_no_ref as 		(
			-- vyber zaznamu z withu w_config, ktere nejsou referenci [config_query != 500]
			select w_config.* from w_config
			where w_config.config_query is distinct from 500
			)
,w_ref as 		(
			-- vyber zaznamu z withu w_config, ktere jsou referecni [config_query = 500]
			select w_config.* from w_config
			where w_config.config_query = 500
			)
,w_data_no_ref as 	(
			-- vyber dat z tabulky t_aux_total pro zaznamy z withu w_no_ref,
			-- kde vyber lze primo provest pres atribut config = id z withu
			-- w_no_ref a HLAVNE estimation_cell is not null
			select
				t_aux_total.id,
				t_aux_total.config as config_res,
				t_aux_total.estimation_cell,
				t_aux_total.aux_total,
				t_aux_total.ext_version,
				t_aux_total.gui_version
			from @extschema@.t_aux_total
			inner join w_no_ref
			on t_aux_total.config = w_no_ref.id
			and t_aux_total.estimation_cell is not null
			)
-------------------------------------------------------------------------------
,w_data_ref as 		(
			-- vyber dat z tabulky t_aux_total pro zaznamy z withu w_ref
			select
				t_aux_total.id,
				w_ref.id as config_res,
				t_aux_total.estimation_cell,
				t_aux_total.aux_total,
				t_aux_total.ext_version,
				t_aux_total.gui_version
			from @extschema@.t_aux_total 
			inner join w_ref
			on t_aux_total.config::character varying = w_ref.categories
			order by w_ref.id
			)
-------------------------------------------------------------------------------
,w_no_ref_max as 	(
			-- z withu w_data_nor_ref a z withu w_data_ref se musi pro unikatni klic,
			-- resp pro unikatni kombinaci config, estimation_cell, ext_version
			-- se musi vybrat zaznam pro nejaktualnejsi EXT_VERSION
			select config_res, estimation_cell, max(ext_version) as max_ext_version
			from w_data_no_ref group by config_res, estimation_cell
			)
-------------------------------------------------------------------------------
,w_ref_max as 		(
			select config_res, estimation_cell, max(ext_version) as max_ext_version
			from w_data_ref group by config_res, estimation_cell
			)
-------------------------------------------------------------------------------
,w_data_no_ref_res as 	(
			select
				w_data_no_ref.config_res as config,
				w_data_no_ref.estimation_cell,
				w_data_no_ref.aux_total
			from w_data_no_ref inner join w_no_ref_max
			on w_data_no_ref.config_res = w_no_ref_max.config_res
			and w_data_no_ref.estimation_cell = w_no_ref_max.estimation_cell
			and w_data_no_ref.ext_version = w_no_ref_max.max_ext_version
			)
-------------------------------------------------------------------------------
,w_data_ref_res as 	(
			select
				w_data_ref.config_res as config,
				w_data_ref.estimation_cell,
				w_data_ref.aux_total
			from w_data_ref inner join w_ref_max
			on w_data_ref.config_res = w_ref_max.config_res
			and w_data_ref.estimation_cell = w_ref_max.estimation_cell
			and w_data_ref.ext_version = w_ref_max.max_ext_version
			)
-------------------------------------------------------------------------------
,w_data_res as 		(
			select * from w_data_no_ref_res union all
			select * from w_data_ref_res
			)
-------------------------------------------------------------------------------
select
	t_config_collection.auxiliary_variable,
	t_config.auxiliary_variable_category,
	--w_data_res.config,
	w_data_res.estimation_cell,
	w_data_res.aux_total
from 
	w_data_res

	left join @extschema@.t_config on w_data_res.config = t_config.id
	left join @extschema@.t_config_collection on t_config.config_collection = t_config_collection.id
	
order by w_data_res.config, w_data_res.estimation_cell
;


ALTER TABLE export_api.v_aux_total OWNER TO adm_nfiesta_gisdata;
COMMENT ON VIEW export_api.v_aux_total IS 'The view displays data from the t_aux_total table.';

GRANT ALL ON TABLE export_api.v_aux_total TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.v_aux_total TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.v_aux_total TO public;

COMMENT ON COLUMN export_api.v_aux_total.auxiliary_variable IS 'Foreign key to superior c_auxiliary_variable.';
COMMENT ON COLUMN export_api.v_aux_total.auxiliary_variable_category IS 'Foreign key to c_auxiliary_variable_category.';
COMMENT ON COLUMN export_api.v_aux_total.estimation_cell IS 'Foreign key to superior c_estimation cell.';
COMMENT ON COLUMN export_api.v_aux_total.aux_total IS 'Total of auxilliary local density.';

-- </view>


-- v_estimation_cell_hierarchy

-- <view name="v_estimation_cell_hierarchy" schema="export_api" src="views/export_api/v_estimation_cell_hierarchy.sql">
create or replace view export_api.v_estimation_cell_hierarchy as
	with w_data as (
	select
		c_estimation_cell.id as c_estimation_cell__id,
		c_estimation_cell.label as c_estimation_cell__label,
		c_estimation_cell_sup.id as c_estimation_cell_sup__id,
		c_estimation_cell_sup.label as c_estimation_cell_sup__label,
		c_estimation_cell_sup.estimation_cell_collection as c_estimation_cell_sup__estimation_cell_collection
	from @extschema@.cm_f_a_cell
	inner join @extschema@.f_a_cell 						on cm_f_a_cell.cell = f_a_cell.gid
	inner join @extschema@.c_estimation_cell 					ON c_estimation_cell.id = f_a_cell.estimation_cell
	inner join @extschema@.f_a_cell 		as f_a_cell_sup 		on cm_f_a_cell.cell_sup = f_a_cell_sup.gid
	inner join @extschema@.c_estimation_cell 	as c_estimation_cell_sup 	ON c_estimation_cell_sup.id = f_a_cell_sup.estimation_cell
	group by 
		c_estimation_cell.id, 
		c_estimation_cell.label, 
		c_estimation_cell_sup.id, 
		c_estimation_cell_sup.label, 
		c_estimation_cell_sup.estimation_cell_collection
	)
	select 
		c_estimation_cell__id as cell,
		/*c_estimation_cell__label,
		c_estimation_cell_sup__estimation_cell_collection,
		array_agg(c_estimation_cell_sup__id) as agg__c_estimation_cell_sup__id,
		array_agg(c_estimation_cell_sup__label) as agg__c_estimation_cell_sup__label,*/
		(array_agg(c_estimation_cell_sup__id))[1] as cell_superior
	from w_data
	group by 
		c_estimation_cell__id, 
		c_estimation_cell__label, 
		c_estimation_cell_sup__estimation_cell_collection
	having count(*) = 1
	order by c_estimation_cell__id, c_estimation_cell_sup__estimation_cell_collection
;

ALTER TABLE export_api.v_estimation_cell_hierarchy OWNER TO adm_nfiesta_gisdata;
COMMENT ON VIEW export_api.v_estimation_cell_hierarchy IS 'View providing hierarchy of estimation cells to nfi_esta ETL process.';

GRANT SELECT ON TABLE export_api.v_estimation_cell_hierarchy TO public;
GRANT SELECT ON TABLE export_api.v_estimation_cell_hierarchy TO app_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.v_estimation_cell_hierarchy TO adm_nfiesta_gisdata;

COMMENT ON COLUMN export_api.v_estimation_cell_hierarchy.cell IS 'Foreign key to c_estimation cell.';
COMMENT ON COLUMN export_api.v_estimation_cell_hierarchy.cell_superior IS 'Foreign key to superior c_estimation cell.';


-- </view>
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
-- FUNCTIONS
---------------------------------------------------------------------------------------------------;
-- <function name="fn_aux_total_before_insert_app" schema="extschema" src="functions/extschema/fn_aux_total_before_insert_app.sql">
-- Function: @extschema@.fn_aux_total_before_insert_app()

-- DROP FUNCTION @extschema@.fn_aux_total_before_insert_app();

CREATE OR REPLACE FUNCTION @extschema@.fn_aux_total_before_insert_app()
  RETURNS trigger AS
$BODY$
	DECLARE
		_ext_version_label_system	text;
		_ext_version_id_system		integer;
		_ext_version_label		character varying;
	BEGIN
		-------------------------------------------------------------------------
		-- cast SYSTEMOVA --
		-------------------------------------------------------------------------
		SELECT extversion FROM pg_extension
		WHERE extname = 'nfiesta_gisdata'
		INTO _ext_version_label_system;
			
		IF _ext_version_label_system IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 01: fn_aux_total_before_insert_app: V systemove tabulce pg_extension nenalezena zadna verze extenze nfiesta_gisdata!';
		END IF;
	
		SELECT id FROM @extschema@.c_ext_version
		WHERE label = _ext_version_label_system
		INTO _ext_version_id_system;
	
		IF _ext_version_id_system IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 02: fn_aux_total_before_insert_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
		END IF;
		-------------------------------------------------------------------------
		-- cast INSERTOVANA
		-------------------------------------------------------------------------
		SELECT label FROM @extschema@.c_ext_version WHERE id = NEW.ext_version
		INTO _ext_version_label;

		IF _ext_version_label IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 03: fn_aux_total_before_insert_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici insertovane verzi % extenze nfiesta_gisdata!',_ext_version_label;
		END IF;
		-------------------------------------------------------------------------
		-- cast KONTROLY
		-------------------------------------------------------------------------
		IF NEW.ext_version > _ext_version_id_system
		THEN
			RAISE EXCEPTION 'Chyba 04: fn_aux_total_before_insert_app: Insertovana verze extenze (ext_version = %) nesmi byt novejsi, nez je prave aktualni systemova verze (%) extenze nfiesta_gisdata!',_ext_version_label,_ext_version_label_system;
		END IF;
		-------------------------------------------------------------------------
		RETURN NEW;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION @extschema@.fn_aux_total_before_insert_app() OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_aux_total_before_insert_app() TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_aux_total_before_insert_app() TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_aux_total_before_insert_app() TO public;

COMMENT ON FUNCTION @extschema@.fn_aux_total_before_insert_app() IS
'Funkce provádí kontrolu importovaného záznamu verze extenze (ext_version). Spouští se triggerem before insert tabulky t_aux_total.';

-- </function>


-- <function name="fn_get_aux_total_app" schema="extschema" src="functions/extschema/fn_get_aux_total_app.sql">
-- Function: @extschema@.fn_get_aux_total_app(integer, integer, integer)

-- DROP FUNCTION @extschema@.fn_get_aux_total_app(integer, integer, integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total_app(
    IN _config_id integer,
    IN _estimation_cell integer,
    IN _gid integer)
  RETURNS TABLE(estimation_cell integer, config integer, aux_total double precision, cell integer, ext_version integer) AS
$BODY$
DECLARE
	_config_collection			integer;
	_config_query				integer;
	_config_function			integer;
	_estimation_cell_exit			integer;
	_function_aux_total			text;
	_categories				character varying;
	_categories_sum				double precision;
	_res_aux_total				double precision;
	_estimation_cell_area			double precision;	
	_complete				character varying;
	_complete_sum				double precision;
	_aux_total				double precision;
	_estimation_cell_collection		integer;
	_estimation_cell_collection_lowest	integer;
	_estimation_cell_result			integer;
	_check_pocet				integer;
	_gid_result				integer;	
	_q					text;

	_ext_version_label_system		text;
	_ext_version_current			integer;
BEGIN
	--------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;	
	
	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	-- proces ziskani aktualni extenze nfiesta_gisdata
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_total_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_total_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;	
	--------------------------------------------------------------------------------------------
	-- proces ziskani potrebnych hodnot z konfigurace
	SELECT
		t.config_collection,
		t.config_query
	FROM
		@extschema@.t_config AS t
	WHERE
		id = _config_id
	INTO
		_config_collection,
		_config_query;
	--------------------------------------------------------------------------------------------
	IF _config_collection IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_total_app: Argument _config_collection nesmi byt NULL!';
	END IF;	

	IF _config_query IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_total_app: Argument _config_query nesmi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	IF _config_query = 500
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_total_app: Vypocet nelze provest pro config_query = %!',_config_query;
	END IF;
	--------------------------------------------------------------------------------------------
	IF _gid IS NOT NULL AND _config_query = ANY(array[200,300,400])
	THEN
		RAISE EXCEPTION 'Chyba 08: fn_get_aux_total_app: Vstupni argument _gid musi byt NULL!';
	END IF;
	--------------------------------------------------------------------------------------------
	-- proces ziskani potrebnych hodnot z kolekce
	SELECT tcc.config_function FROM @extschema@.t_config_collection AS tcc
	WHERE tcc.id = _config_collection
	INTO _config_function;
	--------------------------------------------------------------------------------------------
	IF _config_function IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_total_app: Argument _config_function nesmi byt NULL!';
	END IF;	
	--------------------------------------------------------------------------------------------
	-- kontrola zdali pro zadanou konfiguraci a zadanou _estimation_cell
	-- uz je ci neni hodnota aux_total vypocitana a jestli neni nahodou duplicitni
	-- => toto by mel hlidat bud trigger nebo check-constraint v tabulce t_aux_total
	-- => nebo je implementovano jiz ve funkci fn_get_gids4aux_total
	--------------------------------------------------------------------------------------------	
	--------------------------------------------------------------------------------------------
	-- rozhodovaci proces co funkce fn_get_aux_total_app bude delat:
	CASE
		WHEN (_config_query = 100) -- zakladni with [vypocet z GIS vrstvy]
		THEN
			-- _gid zde nesmi byt hodnota 0 => to je pro identifikaci pro sumarizaci
			-- _calculated zde muze byt jen FALSE => JIRKOVA aplikace tuto funkci bude poustet jen na gidy, 
			-- ktere nejsou doposud vypocitany, nebo pokud se jedna o prepocet, a provede insert do t_aux_total
			-- pokud se    jedna o gid zakladu, pak do vystupu musi jit estimation_cell zakladu
			-- pokud se ne-jedna o gid zakladu, pak do vystupu pujde estimation_cell co je zde na vstupu

			IF _gid > 0
			THEN
				-- zjisteni estimation_cell do vystupu teto funkce
				SELECT fac.estimation_cell FROM @extschema@.f_a_cell AS fac WHERE fac.gid = _gid
				INTO _estimation_cell_exit;

				IF _estimation_cell_exit IS NULL
				THEN
					RAISE EXCEPTION 'Chyba 10: fn_get_aux_total_app: Pro gid = % nenalezeno estimation_cell v tabulce f_a_cell!',_gid;
				END IF;
				
				-- povolen vypocet aux_total hodnoty pro vstupni gid [zakladni protinani]
				CASE
					WHEN (_config_function = 100)	-- vector
					THEN
						_function_aux_total := 'fn_get_aux_total_vector_app';

					WHEN (_config_function = 200)	-- raster
					THEN
						_function_aux_total := 'fn_get_aux_total_raster_app';

					WHEN (_config_function = 300)	-- vector_comb
					THEN
						_function_aux_total := 'fn_get_aux_total_vector_comb_app';

					WHEN (_config_function = 400)	-- raster_comb
					THEN
						_function_aux_total := 'fn_get_aux_total_raster_comb_app';
					ELSE
						RAISE EXCEPTION 'Chyba 11: fn_get_aux_total_app: Neznama hodnota parametru _config_function [%].',_config_function;
				END CASE;
			ELSE
				RAISE EXCEPTION 'Chyba 12: fn_get_aux_total_app: Nepovolena kombinace vstupnich argumentu [_config_id = %, _estimation_cell = %, _gid = %] pro vypocet aux_total hodnoty pro config_query 100!',_config_id,_estimation_cell,_gid;
			END IF;

		WHEN (_config_query = ANY(array[200,300,400]))
		THEN
			-- 200 [soucet existujicich kategorii]
			-- 300 [doplnek do rozlohy vypocetni bunky]
			-- 400 [doplnek do rozlohy existujici kategorie]
			
			_function_aux_total := NULL::text;

			IF _gid IS NULL
			THEN
				-- povolen vypocet aux_total hodnoty pro soucet/doplnky

				_estimation_cell_exit := _estimation_cell;

				--raise notice '_estimation_cell_exit:%',_estimation_cell_exit;

				-- 1. zde se nejpreve z konfigurace zjisti _categories
				SELECT tc.categories FROM @extschema@.t_config AS tc WHERE tc.id = _config_id
				INTO _categories;

				--raise notice '_categories:%',_categories;

				-- 2. potom se pro _categories provede kontrola zda v tabulce t_aux_total vubec _categories existuji
				-- => toto uz je implementovano ve funkci fn_get_gids4aux_total

				-- 3. zde se provede suma za _categories pro danou _estimation_cell
				EXECUTE '
				SELECT sum(t.aux_total) FROM @extschema@.t_aux_total AS t
				WHERE t.estimation_cell = $1
				AND t.config IN (SELECT unnest(array['||_categories||']))
				AND t.ext_version = $2
				'
				USING _estimation_cell, _ext_version_current
				INTO _categories_sum;

				--raise notice '_categories_sum: %',_categories_sum;

				-- 4. proces ziskani _res_aux_total pro jednotlive varianty _config_query 200,300,400
				CASE
				WHEN _config_query = 200	-- soucet
				THEN
					_res_aux_total := _categories_sum;
					
				WHEN _config_query = 300	-- doplnek do rozlohy vypocetni bunky [doplnek pro danou estimation_cell]
				THEN
					-- zjisteni plochy pro danou estimation_cell z tabulky f_a_cell
					SELECT sum(ST_Area(fac.geom))/10000.0 FROM @extschema@.f_a_cell AS fac
					WHERE fac.estimation_cell = _estimation_cell
					INTO _estimation_cell_area;

					IF _estimation_cell_area IS NULL
					THEN
						RAISE EXCEPTION 'Chyba 13: fn_get_aux_total_app: Pro zadanou _estimation_cell = % nezjistena plocha z tabulky f_a_cell!',_estimation_cell;
					END IF;

					_res_aux_total := _estimation_cell_area - _categories_sum;

				WHEN _config_query = 400
				THEN
					-- 1. zde se nejpreve z konfigurace zjisti complete
					SELECT tc.complete FROM @extschema@.t_config AS tc WHERE tc.id = _config_id
					INTO _complete;

					-- 2. potom se pro _complete provede kontrola zda v tabulce t_aux_total vubec existuje
					-- => toto uz je implementovano ve funkci fn_get_gids4aux_total

					-- 3. zjisteni sumy za _complete;
					EXECUTE '
					SELECT sum(t.aux_total) FROM @extschema@.t_aux_total AS t
					WHERE t.estimation_cell = $1
					AND t.config IN (SELECT unnest(array['||_complete||']))
					AND t.ext_version = $2
					'
					USING _estimation_cell, _ext_version_current
					INTO _complete_sum;

					_res_aux_total := _complete_sum - _categories_sum;	
				ELSE
					RAISE EXCEPTION 'Chyba 14: fn_get_aux_total_app: Pro _config_query = % doposud v tele funkce neprovedena implemetace procesu ziskani hodnoty _res_aux_total!',_config_query;
				END CASE;

			ELSE
				RAISE EXCEPTION 'Chyba 15: fn_get_aux_total_app: Nepovolena kombinace vstupnich argumentu [_config_id = %, _estimation_cell = %, _gid = %, _calculated = %] pro vypocet aux_total hodnoty pro config_query 200,300 nebo 400!',_config_id,_estimation_cell,_gid,_calculated;
			END IF;
		ELSE
			RAISE EXCEPTION 'Chyba 16: fn_get_aux_total_app: Neznama hodnota parametru _config_query [%].',_config_query;
	END CASE;
	--------------------------------------------------------------------------------------------
	IF _function_aux_total IS NOT NULL
	THEN
		EXECUTE 'SELECT @extschema@'||_function_aux_total||'($1,$2)'
		USING _config_id, _gid
		INTO _aux_total;
	ELSE
		_aux_total := _res_aux_total;
	END IF;
	--------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	-- zjisteni do jake collection patri zadana vstupni estimation_cell
	SELECT cec.estimation_cell_collection FROM @extschema@.c_estimation_cell  AS cec WHERE cec.id = _estimation_cell
	INTO _estimation_cell_collection;
	--------------------------------------------------------------------------------------------
	-- zjisteni estimation_cell_collection_lowest pro zadanou _estimation_cell_collection
	SELECT cecc.estimation_cell_collection_lowest FROM @extschema@.cm_estimation_cell_collection AS cecc
	WHERE cecc.estimation_cell_collection = _estimation_cell_collection
	INTO _estimation_cell_collection_lowest;
	--------------------------------------------------------------------------------------------
	IF _config_query = 100
	THEN
		IF _estimation_cell_collection != _estimation_cell_collection_lowest	-- VSTUPNI estimation_cell NENI zaklad [pozn. gidy pro protinani jsou vzdy zaklad]
		THEN
			-- ale pokud funkce fn_get_gids4aux_total_app vratila gid pro danou vstupni estimation_cell => tzn. ze zakladni gid jeste neni v t_aux_total
			-- a musel se jiz drive v kodu pro gid dohledat estimation_cell
			-- napr. chci TREBIC a zakladni gidy pro tuto estimation_cell nejsou jeste ulozeny v t_aux_total
			-- vystup za NUTS1-4 a rajonizace => pujde pres step 2 a sumarizaci
			-- vystup za OPLO => pujde pres step 2 a sumarizaci

			IF _gid > 0
			THEN		
				_estimation_cell_result := _estimation_cell_exit;
			ELSE
				RAISE EXCEPTION 'Chyba 17: fn_get_aux_total_app: Jde-li o config_query = 100, pak gid nesmi byt nikdy NULL nebo hodnota 0!';
			END IF;
		ELSE
			-- VSTUPNI estimation_cell JE zakladd
			
			-- zjisteni poctu geometrii, ktere tvori vstupni estimation_cell
			SELECT count(fac.gid) FROM @extschema@.f_a_cell AS fac WHERE fac.estimation_cell = _estimation_cell
			INTO _check_pocet;

			IF _check_pocet IS NULL
			THEN
				RAISE EXCEPTION 'Chyba 18: fn_get_aux_total_app: Pro estimation_cell = % nenalezeny geometrie v tabulce f_a_cell!',_estimation_cell;
			END IF;

			IF _check_pocet = 1 -- vstupni estimation_cell uz neni nijak geometricky rozdrobena v f_a_cell
			THEN
				_estimation_cell_result := _estimation_cell_exit;
			ELSE
				_estimation_cell_result := NULL::integer;
			END IF;
		END IF;

		_gid_result := _gid;
	ELSE
		IF _config_query = ANY(array[200,300,400])
		THEN
			_estimation_cell_result := _estimation_cell_exit;
			_gid_result := NULL::integer;
		ELSE
			RAISE EXCEPTION 'Chyba 19: fn_get_aux_total_app: Neznama hodnota config_query = %!',_config_query;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	_q := 'SELECT $1,$2,$3,$4,$5';
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||'' USING _estimation_cell_result,_config_id,_aux_total,_gid_result,_ext_version_current;
	--------------------------------------------------------------------------------------------
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total_app(integer,integer,integer) IS
'Funkce vraci vypocitanou hodnotu aux_total pro zadanou konfiguraci (config_id) a celu (estimation_cell) nebo gid.';

-- </function>


-- <function name="fn_get_aux_total4estimation_cell_app" schema="extschema" src="functions/extschema/fn_get_aux_total4estimation_cell_app.sql">
-- DROP function @extschema@.fn_get_aux_total4estimation_cell_app (integer,integer,integer,boolean)

CREATE OR REPLACE FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app
	(
	_config_id			integer,
	_estimation_cell		integer,
	_gid				integer,
	_recount			boolean DEFAULT FALSE
	)
RETURNS TABLE
	(
        estimation_cell			integer,
        config				integer, 
        aux_total			double precision,
        cell				integer,
        ext_version			integer
        )
AS
$BODY$
DECLARE
	_estimation_cell_collection		integer;
	_estimation_cell_collection_lowest	integer;
	_check_estimation_cell			boolean;
	_check_pocet				integer;
	_gids4estimation_cell			integer[];
	_gids4estimation_cell_lowest		integer[];
	_gids_in_t_aux_total_check		integer;
	_aux_total				double precision;
	_q					text;

	_ext_version_label_system		text;
	_ext_version_current			integer;
BEGIN
	-------------------------------------------------------------------------------------------
	IF _config_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_get_aux_total4estimation_cell_app: Vstupni argument _config_id nesmi byt NULL!';
	END IF;

	IF _estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_get_aux_total4estimation_cell_app: Vstupni argument _estimation_cell nesmi byt NULL!';
	END IF;

	IF _gid IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_get_aux_total4estimation_cell_app: Vstupni argument _gid nesmi byt NULL!';
	END IF;

	IF _gid != 0
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_get_aux_total4estimation_cell_app: Vstupni argument _gid musi byt hodnota 0!';
	END IF;

	IF _recount IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_get_aux_total4estimation_cell_app: Vstupni argument _recount nesmi byt NULL!';
	END IF;	
	-------------------------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------
	-- proces ziskani aktualni systemove extenze nfiesta_gisdata
	
	SELECT extversion FROM pg_extension WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;

	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 06: fn_get_aux_total4estimation_cell_app: V systemove tabulce pg_extension nenalezena zadna verze pro extenzi nfiesta_gisdata!';
	END IF;

	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_current;
	
	IF _ext_version_current IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 07: fn_get_aux_total4estimation_cell_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;		
	-------------------------------------------------------------------------------------------
	-- kontrola, ze estimation_cell:
	-- 1. nesmi byt zakladni stavebni jednotka [ZSJ] => vyjimka plati jen pro viz. bod 2
	-- 2. muze to byt ZSJ ale pocet geometrii, ktery tvori onu ZSJ musi byt vice nez 1
	--------------------------------------------------------------------------------------------
	-- zjisteni do jake estimation_cell_collection patri zadana vstupni estimation_cell
	SELECT cec.estimation_cell_collection FROM @extschema@.c_estimation_cell AS cec WHERE cec.id = _estimation_cell
	INTO _estimation_cell_collection;
	--------------------------------------------------------------------------------------------
	-- zjisteni estimation_cell_collection_lowest pro zjistenou _estimation_cell_collection
	SELECT cecc.estimation_cell_collection_lowest FROM @extschema@.cm_estimation_cell_collection AS cecc
	WHERE cecc.estimation_cell_collection = _estimation_cell_collection
	INTO _estimation_cell_collection_lowest;
	--------------------------------------------------------------------------------------------
	IF _estimation_cell_collection != _estimation_cell_collection_lowest
	THEN
		-- vstupni estimation_cell neni ZSJ => splnena 1. podminka kontroly
		_check_estimation_cell := TRUE;
	ELSE
		-- vstupni estimation_cell je ZSJ
		
		-- zjisteni poctu geometrii, ktere tvori vstupni estimation_cell, ktera je ZSJ
		SELECT count(fac.gid) FROM @extschema@.f_a_cell AS fac
		WHERE fac.estimation_cell = _estimation_cell
		INTO _check_pocet;

		IF _check_pocet IS NULL
		THEN
			RAISE EXCEPTION 'Chyba 08: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = % nenalezena zadna geometrie v tabulce f_a_cell!',_estimation_cell;
		END IF;

		IF _check_pocet = 1 -- vstupni estimation_cell (ZSJ) uz neni nijak geometricky rozdrobena v f_a_cell na mensi casti
		THEN
			_check_estimation_cell := FALSE;
		ELSE
			_check_estimation_cell := TRUE;
		END IF;
	END IF;
	--------------------------------------------------------------------------------------------
	IF _check_estimation_cell = FALSE
	THEN
		RAISE EXCEPTION 'Chyba 09: fn_get_aux_total4estimation_cell_app: Zadanou estimation_cell = % neni mozno sumarizovat. Jedna se totiz o nejnizsi geografickou uroven, ktera neni geometricky rozdrobena na mensi casti!',_estimation_cell;
	END IF;
	--------------------------------------------------------------------------------------------
	-- kontrola zda jiz pro zadanou estimation_cell, config_id a ext_version neni uz vypocitana
	-- hodnota aux_total v tabulce t_aux_total
	-- kontrola se provadi pri VYPOCTU i PREPOCTU [puvodne zde bylo jen pri VYPOCTU]
	-- jelikoz je u vstupnich argumentu ponechan _recount, pak IF jsem upravil nasledovne
	IF _recount = ANY(array[TRUE,FALSE])
	THEN
		IF	(
			SELECT count(tat.*) > 0
			FROM @extschema@.t_aux_total AS tat
			WHERE tat.config = _config_id
			AND tat.estimation_cell = _estimation_cell
			AND tat.ext_version = _ext_version_current
			)
		THEN
			RAISE EXCEPTION 'Chyba 10: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = %, config_id = % a ext_version = % jiz existuje hodnota aux_total v tabulce t_aux_total. Sumarizace neni mozna!',_estimation_cell,_config_id,_ext_version_label_system;
		END IF;
	END IF;
	-------------------------------------------------------------------------------------------
	-- zjisteni gidu (ZSJ) z tabulky f_a_cell, ktere tvori zadanou _estimation_cell
	SELECT array_agg(fac.gid ORDER BY fac.gid) FROM @extschema@.f_a_cell AS fac
	WHERE fac.estimation_cell = _estimation_cell
	INTO _gids4estimation_cell;

	IF _gids4estimation_cell IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 11: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = % nenalezeny zadne zaznamy v tabulce f_a_cell!',_estimation_cell;
	END IF;

	-- volani funkce fn_get_lowest_gids [funkce si saha do mapovaci tabulky cm_f_a_cell]
	-- funkce by mela vratit seznam gidu nejnizsi urovne (ZSJ), ktere tvori zadany seznam vyssich gidu
	_gids4estimation_cell_lowest := @extschema@.fn_get_lowest_gids_app(_gids4estimation_cell);
	-------------------------------------------------------------------------------------------
	-- kontrola zda v tabulce t_aux_total jsou pro sumarizaci vsechny gidy nejnizsi urovne
	-- pro config_id a ext_version
	WITH
	w1 AS	(SELECT unnest(_gids4estimation_cell_lowest) AS gids),
	w2 AS	(
		SELECT
			tat.config,
			tat.cell
		FROM
			@extschema@.t_aux_total AS tat
		WHERE
			tat.config = _config_id
		AND
			tat.cell IN (SELECT gids FROM w1)
		AND
			tat.ext_version = _ext_version_current
		),
	w3 AS	(
		SELECT
			w1.gids,
			w2.cell
		FROM
			w1 LEFT JOIN w2
		ON
			w1.gids = w2.cell
		)
	SELECT
		count(*) FROM w3 WHERE w3.cell IS NULL
	INTO
		_gids_in_t_aux_total_check;
	-------------------------------------------------------------------------------------------
	IF _gids_in_t_aux_total_check > 0
	THEN
		RAISE EXCEPTION 'Chyba 12: fn_get_aux_total4estimation_cell_app: Pro estimation_cell = %, config_id = % a ext_version = % neni v tabulce t_aux_total kompletni seznam hodnot aux_total pro sumarizaci!',_estimation_cell,_config_id,_ext_version_label_system;
	END IF;
	-------------------------------------------------------------------------------------------
	-- vypocet hodnoty aux_total pro danou estimation_cell
	WITH
	w1 AS	(SELECT unnest(_gids4estimation_cell_lowest) AS gids),
	w2 AS	(
		SELECT
			tat.cell,
			tat.aux_total
		FROM
			@extschema@.t_aux_total AS tat
		WHERE
			tat.config = _config_id
		AND
			tat.cell IN (SELECT gids FROM w1)
		AND
			tat.ext_version = _ext_version_current
		)
	SELECT
		sum(w2.aux_total) AS aux_total
	FROM
		w2
	INTO
		_aux_total;
	-------------------------------------------------------------------------------------------
	/*
	IF _aux_total < 0.0
	THEN
		RAISE EXCEPTION 'Chyba 13: fn_get_aux_total4estimation_cell_app: Pro zadanou hodnotu (_config_id = %), (_estimation_cell = %) ve verzi (ext_version = %) je aux_total ZAPORNA hodnota !', _config_id,_estimation_cell,_ext_version_label_system;	
	END IF;
	*/
	-------------------------------------------------------------------------------------------
	_q := 'SELECT $1,$2,$3,NULL::integer,$4';
	--------------------------------------------------------------------------------------------
	RETURN QUERY EXECUTE ''||_q||'' USING _estimation_cell,_config_id,_aux_total,_ext_version_current;
	--------------------------------------------------------------------------------------------
END ;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) TO public;

COMMENT ON FUNCTION @extschema@.fn_get_aux_total4estimation_cell_app(integer,integer,integer,boolean) IS
'Funkce vraci hodnotu aux_total pro zadanou estimation_cell a config_id.';

-- </function>


-- <function name="fn_check_versions_app" schema="extschema" src="functions/extschema/fn_check_versions_app.sql">
---------------------------------------------------------------------------------------------------
-- fn_check_versions_app
---------------------------------------------------------------------------------------------------
-- DROP FUNCTION @extschema@.fn_check_versions_app(integer);

CREATE OR REPLACE FUNCTION @extschema@.fn_check_versions_app
(
	_gui_version_id		integer
)
RETURNS boolean AS
$BODY$
DECLARE
	_gui_version_label		character varying;
	_ext_version_label_system	text;
	_ext_version_id_system		integer;
	_ext_version_id_c_max		integer;
	_ext_version_label_c_max	character varying;
	_ext_vesion_id_max_mapping	integer;
BEGIN
	---------------------------------------------------
	IF _gui_version_id IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 01: fn_check_versions_app: Vstupni argument _gui_version_id nesmi byt NULL!';
	END IF;
	---------------------------------------------------
	-- kontrola existence GUI verze v ciselniku
	-- soucasne i ziskani labelu
	SELECT label FROM @extschema@.c_gui_version
	WHERE id = _gui_version_id
	INTO _gui_version_label;

	IF _gui_version_label IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 02: fn_check_versions_app: Pro vstupni argument _gui_version_id = % nenalezen zaznam v ciselniku c_gui_version!',_gui_version_id;
	END IF;
	---------------------------------------------------
	-- kontrola existence systemove verze EXTENZE v ciselniku
	SELECT extversion FROM pg_extension
	WHERE extname = 'nfiesta_gisdata'
	INTO _ext_version_label_system;
	
	IF _ext_version_label_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 03: fn_check_versions_app: V systemove tabulce pg_extension nenalezena zadna verze extenze nfiesta_gisdata!';
	END IF;
	
	SELECT id FROM @extschema@.c_ext_version
	WHERE label = _ext_version_label_system
	INTO _ext_version_id_system;
	
	IF _ext_version_id_system IS NULL
	THEN
		RAISE EXCEPTION 'Chyba 04: fn_check_versions_app: V ciselniku c_ext_version nenalezen zaznam odpovidajici systemove verzi % extenze nfiesta_gisdata!',_ext_version_label_system;
	END IF;
	---------------------------------------------------
	-- kontrola ze systemova verze extenze je tou
	-- nejaktualnejsi co je v ciselniku
	SELECT max(id) FROM @extschema@.c_ext_version
	INTO _ext_version_id_c_max;

	SELECT label FROM @extschema@.c_ext_version
	WHERE id = _ext_version_id_c_max
	INTO _ext_version_label_c_max;

	IF _ext_version_id_system != _ext_version_id_c_max
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_check_versions_app: Systemova (databazova) verze % extenze nfiesta_gisdata neodpovida nejaktualnejsi verzi % v ciselniku c_ext_version!',_ext_version_label_system, _ext_version_label_c_max;
	END IF;
	---------------------------------------------------
	-- kontrola v mapovaci tabulce, ze pro vstupni GUI verzi
	-- odpovida systemova extenze, ktera by mela byt tou nejakutalnejsi
	SELECT max(ext_version) FROM @extschema@.cm_ext_gui_version
	WHERE gui_version = _gui_version_id
	INTO _ext_vesion_id_max_mapping;

	IF _ext_vesion_id_max_mapping != _ext_version_id_c_max
	THEN
		RAISE EXCEPTION 'Chyba 05: fn_check_versions_app: Verze (%) GUI aplikace neni kompatibilni s verzi (%) extenze nfiesta_gisdata',_gui_version_label,_ext_version_label_c_max;
	END IF;
	---------------------------------------------------
	RETURN TRUE;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;

ALTER FUNCTION @extschema@.fn_check_versions_app(integer) OWNER TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_versions_app(integer) TO adm_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_versions_app(integer) TO app_nfiesta_gisdata;
GRANT EXECUTE ON FUNCTION @extschema@.fn_check_versions_app(integer) TO public;

COMMENT ON FUNCTION @extschema@.fn_check_versions_app(integer) IS
'Funkce, ktera provadi kontrolu kompatibility verze GUI aplikace s verzi extenze nfiesta_gisdata. Funkce vraci TRUE pokud je vse kompatibilni, jinak vyjimky funkce.';
---------------------------------------------------------------------------------------------------
-- </function>
---------------------------------------------------------------------------------------------------;






