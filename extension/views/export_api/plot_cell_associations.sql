--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP VIEW IF EXISTS export_api.plot_cell_associations;

-- DDL
CREATE OR REPLACE VIEW export_api.plot_cell_associations AS
SELECT
	t1.country,
	t1.strata_set,
	t1.stratum,
	t1.panel,
	t1.cluster,
	t1.plot,

	t4.label AS estimation_cell_collection,
	t3.label AS estimation_cell,

	NULL::varchar AS comment
FROM
	@extschema@.f_p_plot AS t1
INNER JOIN
	@extschema@.f_a_cell AS t2
ON
	t1.geom && t2.geom AND
	ST_Intersects(t1.geom,t2.geom)
INNER JOIN
	@extschema@.c_estimation_cell AS t3
ON
	t2.estimation_cell = t3.id
INNER JOIN
	@extschema@.c_estimation_cell_collection AS t4
ON
	t3.estimation_cell_collection = t4.id
;

-- authorization
ALTER TABLE export_api.plot_cell_associations OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.plot_cell_associations TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.plot_cell_associations TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.plot_cell_associations TO public;

-- documentation
COMMENT ON VIEW export_api.plot_cell_associations IS 'View with list of plots and their belongings into estimation cells.';

