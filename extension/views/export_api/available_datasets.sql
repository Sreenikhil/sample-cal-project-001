--
-- Copyright 2020, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP VIEW IF EXISTS export_api.available_datasets;

-- DDL
CREATE OR REPLACE VIEW export_api.available_datasets AS
WITH
w_plots AS (
	SELECT
		country,
		strata_set,
		stratum,
		panel,
		cluster,
		plot
	FROM
		@extschema@.f_p_plot
)
	SELECT
		t1.panel,
		NULL::int AS reference_year_set,
		t2.auxiliary_variable_category
	FROM
		w_plots AS t1
	INNER JOIN
		export_api.auxiliary_data AS t2
	ON t1.plot = t2.plot
	GROUP BY
		t1.panel, t2.auxiliary_variable_category
;

-- authorization
ALTER TABLE export_api.available_datasets OWNER TO adm_nfiesta_gisdata;
GRANT ALL ON TABLE export_api.available_datasets TO adm_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.available_datasets TO app_nfiesta_gisdata;
GRANT SELECT ON TABLE export_api.available_datasets TO public;

-- documentation
COMMENT ON VIEW export_api.available_datasets IS 'View declaring for which panel are defined auxiliaries available.';

